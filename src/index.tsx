import * as React from 'react'
import * as ReactDOM from 'react-dom'

// Import Css
import 'antd/dist/antd.min.css'
import './css/theme.css'

// Import Component
import { Button } from 'antd'

class App extends React.Component {
  public render () {
    return (
      <div>
        <Button style={{ textAlign: 'center' }} type='primary'>ABC</Button>
      </div>
    )
  }
}

ReactDOM.render(<App />, document.getElementById('root') as HTMLElement)
